package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.TotalInfected;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TotalInfectedRepository implements ITotalInfectedRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class TotalInfectedRowCallbackHandler implements RowCallbackHandler {

        private List<TotalInfected> totalInfectedList = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int index = 1;
            int id = rs.getInt(index++);
            int totalNumberSick = rs.getInt(index++);
            LocalDateTime lastUpdated = rs.getTimestamp(index).toLocalDateTime();

            TotalInfected totalInfected = new TotalInfected(id, totalNumberSick, lastUpdated);

            totalInfectedList.add(totalInfected);
        }

        public List<TotalInfected> getTotalInfectedList() {
            return totalInfectedList;
        }
    }

    @Override
    public List<TotalInfected> findAll() {
        String sql = "SELECT * FROM totalInfected";
        TotalInfectedRowCallbackHandler rowCallbackHandler = new TotalInfectedRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getTotalInfectedList();
    }

    @Override
    public TotalInfected findOne(Integer id) {
        String sql = "SELECT * FROM totalInfected WHERE id = ?";
        TotalInfectedRowCallbackHandler rowCallbackHandler = new TotalInfectedRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        return rowCallbackHandler.getTotalInfectedList().isEmpty() ? null : rowCallbackHandler.getTotalInfectedList().get(0);
    }
}
