package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Manufacturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManufacturerRepository implements IManufacturerRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class ManufacturerRowCallBackHandler implements RowCallbackHandler {

        private List<Manufacturer> manufacturers = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int index = 1;
            Long id = rs.getLong(index++);
            String name = rs.getString(index++);
            String country = rs.getString(index);

            Manufacturer manufacturer = new Manufacturer(id, name, country);
            manufacturers.add(manufacturer);
        }

        public List<Manufacturer> getManufacturers() {
            return manufacturers;
        }
    }

    @Override
    public List<Manufacturer> findAllManufacturer() {
        String sql = "select * from manufacturers";
        ManufacturerRowCallBackHandler rowCallbackHandler = new ManufacturerRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getManufacturers();
    }

    @Override
    public Manufacturer findOne(Long id) {
        String sql = "select * from manufacturers where id = ?";
        ManufacturerRowCallBackHandler rowCallBackHandler = new ManufacturerRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler, id);
        return rowCallBackHandler.getManufacturers().get(0);
    }

    @Override
    public int save(Manufacturer manufacturer) {
        String sql = "insert into manufacturers (name, country) values (?, ?)";
        return jdbcTemplate.update(sql, manufacturer.getName(), manufacturer.getCountry());
    }

    @Override
    public int update(Manufacturer manufacturer) {
        String sql = "update manufacturers set name = ?, country = ? where id = ?";
        return jdbcTemplate.update(sql, manufacturer.getName(), manufacturer.getCountry(), manufacturer.getId());
    }

    @Transactional
    @Override
    public int delete(Long id) {
        String sql = "delete from manufacturers where id = ?";
        return jdbcTemplate.update(sql, id);
    }
}
