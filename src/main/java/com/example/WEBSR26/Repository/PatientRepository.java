package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PatientRepository implements IPatientRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class PatientRowCallbackHandler implements RowCallbackHandler {

        private List<Patient> patientList = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int index = 1;
            Long patientId = rs.getLong(index++);
            boolean vaccinated = rs.getBoolean(index++);
            Integer doseAdministered = rs.getInt(index++);
            LocalDate lastDoseDate = rs.getObject(index++, LocalDate.class);
            int userId = rs.getInt(index);

            Patient patient = new Patient(patientId, vaccinated, doseAdministered, lastDoseDate, null, userId);

            patientList.add(patient);
        }

        public List<Patient> getPatientList() {
            return patientList;
        }
    }

    @Override
    public List<Patient> findAll() {
        String sql = "SELECT * FROM patients";
        PatientRowCallbackHandler rowCallbackHandler = new PatientRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getPatientList();
    }

    @Override
    public Patient findOne(Integer id) {
        String sql = "SELECT * FROM patients WHERE id = ?";
        PatientRowCallbackHandler rowCallbackHandler = new PatientRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        return rowCallbackHandler.getPatientList().isEmpty() ? null : rowCallbackHandler.getPatientList().get(0);
    }

    @Override
    public int save(Patient patient) {
        String sql = "INSERT INTO patients (vaccinated, doseAdministered, lastDose, userID) VALUES (?, ?, ?, ?)";
        return jdbcTemplate.update(sql, patient.isVaccinated(), patient.getDoseAdministered(), patient.getLastDose(), patient.getUserId());
    }

    @Override
    public int update(Patient patient) {
        String sql = "UPDATE patients SET vaccinated = ?, doseAdministered = ?, lastDose = ?, userID = ? WHERE id = ?";
        return jdbcTemplate.update(sql, patient.isVaccinated(), patient.getDoseAdministered(), patient.getLastDose(), patient.getUserId(), patient.getPatientId());
    }


    @Override
    public int delete(Integer id) {
        String sql = "DELETE FROM patients WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
}
