package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Patient;

import java.util.List;

public interface IPatientRepository {
    Patient findOne(Integer id);

    List<Patient> findAll();

    int save(Patient patient);

    int update(Patient patient);

    int delete(Integer id);
}
