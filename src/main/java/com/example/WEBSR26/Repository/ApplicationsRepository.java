package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Applications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ApplicationsRepository implements IApplicationsRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class ApplicationsRowCallbackHandler implements RowCallbackHandler {

        private List<Applications> applicationsList = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            Long id = rs.getLong("id");
            LocalDateTime dateTime = rs.getTimestamp("date_time").toLocalDateTime();
            int patientId = rs.getInt("patient_id");
            int vaccineId = rs.getInt("vaccine_id");

            Applications application = new Applications(id, dateTime, patientId, vaccineId);
            applicationsList.add(application);
        }

        public List<Applications> getApplicationsList() {
            return applicationsList;
        }
    }

    @Override
    public Applications findOne(Integer id) {
        String sql = "SELECT * FROM applications WHERE id = ?";
        ApplicationsRowCallbackHandler rowCallbackHandler = new ApplicationsRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        List<Applications> applicationsList = rowCallbackHandler.getApplicationsList();
        return applicationsList.isEmpty() ? null : applicationsList.get(0);
    }

    @Override
    public List<Applications> findAll() {
        String sql = "SELECT * FROM applications";
        ApplicationsRowCallbackHandler rowCallbackHandler = new ApplicationsRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getApplicationsList();
    }

    @Override
    public int save(Applications application) {
        String sql = "INSERT INTO applications (date_time, patient_id, vaccine_id) VALUES (?, ?, ?)";
        return jdbcTemplate.update(sql, application.getDateTime(), application.getPatientId(), application.getVaccineId());
    }

    @Override
    public int update(Applications application) {
        String sql = "UPDATE applications SET date_time = ?, patient_id = ?, vaccine_id = ? WHERE id = ?";
        return jdbcTemplate.update(sql, application.getDateTime(), application.getPatientId(), application.getVaccineId(), application.getId());
    }

    @Override
    public int delete(Integer id) {
        String sql = "DELETE FROM applications WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }

    @Override
    public void deleteByPatient(Integer patientId, Integer id) {
        String sql = "DELETE FROM applications WHERE patient_id = ? AND id = ?";
        jdbcTemplate.update(sql, patientId, id);
    }

    @Override
    public List<Applications> searchApplications(String query) {
        String sql = "SELECT * FROM applications WHERE ..."; // Add your search conditions here
        ApplicationsRowCallbackHandler rowCallbackHandler = new ApplicationsRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getApplicationsList();
    }
}
