package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class NewsRepository implements INewsRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class NewsRowCallbackHandler implements RowCallbackHandler {

        private List<News> newsList = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int index = 1;
            int id = rs.getInt(index++);
            String name = rs.getString(index++);
            String content = rs.getString(index++);
            LocalDateTime date = rs.getTimestamp(index).toLocalDateTime();

            News news = new News(id, name, content, date);

            newsList.add(news);
        }

        public List<News> getNewsList() {
            return newsList;
        }
    }

    @Override
    public List<News> findAll() {
        String sql = "select * from news";
        NewsRowCallbackHandler rowCallbackHandler = new NewsRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getNewsList();
    }

    @Override
    public News findOne(Integer id) {
        String sql = "select * from news where id = ?";
        NewsRowCallbackHandler rowCallbackHandler = new NewsRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        return rowCallbackHandler.getNewsList().isEmpty() ? null : rowCallbackHandler.getNewsList().get(0);
    }

    @Override
    public int save(News news) {
        String sql = "insert into news (name, content, date) values (?, ?, ?)";
        return jdbcTemplate.update(sql, news.getName(), news.getContent(), news.getDate());
    }

    @Override
    public int update(News news) {
        String sql = "update news set name = ?, content = ?, date = ? where id = ?";
        return jdbcTemplate.update(sql, news.getName(), news.getContent(), news.getDate(), news.getId());
    }

    @Override
    public int delete(Integer id) {
        String sql = "delete from news where id = ?";
        return jdbcTemplate.update(sql, id);
    }
}
