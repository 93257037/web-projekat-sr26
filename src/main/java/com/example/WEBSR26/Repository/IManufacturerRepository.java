package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Manufacturer;

import java.util.List;

public interface IManufacturerRepository {
    public Manufacturer findOne(Long id);
    public List<Manufacturer> findAllManufacturer();
    public int save(Manufacturer manufacturer);
    public int update (Manufacturer manufacturer);
    public int delete(Long id);
}

