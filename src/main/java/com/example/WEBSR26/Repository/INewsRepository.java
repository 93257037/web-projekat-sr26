package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.News;

import java.util.List;

public interface INewsRepository {
    News findOne(Integer id);

    List<News> findAll();

    int save(News news);

    int update(News news);

    int delete(Integer id);
}
