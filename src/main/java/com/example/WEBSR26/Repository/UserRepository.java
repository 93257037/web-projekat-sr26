package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Role;
import com.example.WEBSR26.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserRepository implements IUserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class UserRowCallBackHandler implements RowCallbackHandler {

        private Map<Long, User> muser = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int index = 1;
            Long id = rs.getLong(index++);
            String name = rs.getString(index++);
            String surName = rs.getString(index++);
            String password = rs.getString(index++);
            String jmbg = rs.getString(index++);
            String email = rs.getString(index++);
            String address = rs.getString(index++);
            String phoneNumber = rs.getString(index++);
            Role userRole = Role.valueOf(rs.getString(index++));
            LocalDateTime regDate = rs.getObject(index++, LocalDateTime.class);
            LocalDate dateOfBirth = rs.getObject(index, LocalDate.class);

            User user = muser.get(id);
            if (user == null) {
                user = new User(id, name, surName, password, jmbg, email, address, phoneNumber, userRole, regDate, dateOfBirth);
                muser.put(id, user);
            }

        }
        public List<User> getUsers() {
            return new ArrayList<>(muser.values());
        }
    }

    @Override
    public User findOne(Long id) {
        String sql = "select * from users where id = ?";
        UserRepository.UserRowCallBackHandler rowCallbackHandler = new UserRepository.UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        return rowCallbackHandler.getUsers().get(0);
    }

    @Override
    public User findOne(String email) {
        String sql =
                "SELECT kor.id, kor.name, kor.surName, kor.password, kor.jmbg,kor.email, kor.address, kor.phoneNumber, kor.role, kor.regdate, kor.dateofbirth " +
                        "FROM users kor " +
                        "WHERE kor.email = ? " +
                        "ORDER BY kor.id";

        UserRowCallBackHandler rowCallbackHandler = new UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, email);

        if (rowCallbackHandler.getUsers().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getUsers().get(0);
    }

    @Override
    public User findOne(String email, String password) {
        String sql =
                "SELECT kor.id, kor.name, kor.surName, kor.password, kor.jmbg,kor.email, kor.address, kor.phoneNumber, kor.role, kor.regdate, kor.dateofbirth " +
                        "FROM users kor " +
                        "WHERE kor.email = ? AND " +
                        "kor.password = ? " +
                        "ORDER BY kor.id";

        UserRowCallBackHandler rowCallbackHandler = new UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, email, password);

        if (rowCallbackHandler.getUsers().size() == 0) {
            return null;
        }
        return rowCallbackHandler.getUsers().get(0);
    }



    @Override
    public List<User> findAll() {
        String sql = "select * from users";
        UserRepository.UserRowCallBackHandler rowCallbackHandler = new UserRepository.UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getUsers();
    }

    @Transactional
    @Override
    public User save(User user) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "insert into users (name, surname, password, jmbg,email, address, phoneNumber, role, regDate, dateOfBirth) values(?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
                PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                preparedStatement.setString(index++, user.getName());
                preparedStatement.setString(index++, user.getSurName());
                preparedStatement.setString(index++, user.getPassword());
                preparedStatement.setString(index++, user.getJmbg());
                preparedStatement.setString(index++, user.getEmail());
                preparedStatement.setString(index++, user.getAddress());
                preparedStatement.setString(index++, user.getPhoneNumber());
                preparedStatement.setString(index++, user.getRole().toString());
                preparedStatement.setString(index++, user.getRegDate().toString());
                preparedStatement.setString(index, user.getDateOfBirth().toString());
                return preparedStatement;
            }
        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(preparedStatementCreator, keyHolder);

        // Set the generated ID to the user object
        user.setId(keyHolder.getKey().longValue());
        return user;
    }


    @Override
    public User update(User user) {
        String sql = "update users set name = ?, surname = ?, password = ?, jmbg = ?, email = ?, address = ?, phoneNumber = ?, role = ?, regDate = ?, dateOfBirth = ? where id = ?";
        int rowsAffected = jdbcTemplate.update(sql, user.getName(), user.getSurName(), user.getPassword(), user.getJmbg(), user.getEmail(), user.getAddress(), user.getPhoneNumber(), user.getRole().toString(), Timestamp.valueOf(user.getRegDate()), Date.valueOf(user.getDateOfBirth()), user.getId());

        if (rowsAffected == 1) {
            return user;
        } else {
            return null;
        }
    }


    @Transactional
    @Override
    public User delete(Long id) {
        String sql = "delete from users where id = ?";
        int rowsAffected = jdbcTemplate.update(sql, id);

        if (rowsAffected == 1) {
            return new User(id, null, null, null, null, null, null, null, null, null, null, false);
        } else {
            return null;
        }
    }


    @Override
    public User findByEmailAndPassword(String email, String password) {
        String sql = "select * from users where email = ? and password = ?";

        UserRepository.UserRowCallBackHandler rowCallbackHandler = new UserRepository.UserRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, email, password);
        if (rowCallbackHandler.muser.size() == 0) {
            return null;
        } else {
            return rowCallbackHandler.getUsers().get(0);
        }
    }
}
