package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.SickNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SickNewsRepository implements ISickNewsRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class SickNewsRowCallbackHandler implements RowCallbackHandler {

        private List<SickNews> sickNewsList = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int id = rs.getInt("id");
            int numberSick = rs.getInt("numberSick");
            int tested = rs.getInt("tested");
            int hospitalized = rs.getInt("hospitalized");
            int onRespirator = rs.getInt("onRespirator");
            LocalDateTime date = rs.getTimestamp("date").toLocalDateTime();

            SickNews sickNews = new SickNews(id, numberSick, tested, hospitalized, onRespirator, date);
            sickNewsList.add(sickNews);
        }

        public List<SickNews> getSickNewsList() {
            return sickNewsList;
        }
    }

    @Override
    public SickNews findOne(Integer id) {
        String sql = "SELECT * FROM sickNews WHERE id = ?";
        SickNewsRowCallbackHandler rowCallbackHandler = new SickNewsRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        List<SickNews> sickNewsList = rowCallbackHandler.getSickNewsList();
        return sickNewsList.isEmpty() ? null : sickNewsList.get(0);
    }

    @Override
    public List<SickNews> findAll() {
        String sql = "SELECT * FROM sickNews";
        SickNewsRowCallbackHandler rowCallbackHandler = new SickNewsRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getSickNewsList();
    }

    @Override
    public int save(SickNews sickNews) {
        String sql = "INSERT INTO sickNews (numberSick, tested, hospitalized, onRespirator, date) VALUES (?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, sickNews.getNumberSick(), sickNews.getTested(),
                sickNews.getHospitalized(), sickNews.getOnRespirator(), sickNews.getDate());
    }

    @Override
    public int update(SickNews sickNews) {
        String sql = "UPDATE sickNews SET numberSick = ?, tested = ?, hospitalized = ?, onRespirator = ?, date = ? WHERE id = ?";
        return jdbcTemplate.update(sql, sickNews.getNumberSick(), sickNews.getTested(),
                sickNews.getHospitalized(), sickNews.getOnRespirator(), sickNews.getDate(), sickNews.getId());
    }

    @Override
    public int delete(Integer id) {
        String sql = "DELETE FROM sickNews WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
}
