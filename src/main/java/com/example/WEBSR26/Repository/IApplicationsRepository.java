package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Applications;

import java.util.List;

public interface IApplicationsRepository {
    Applications findOne(Integer id);

    List<Applications> findAll();

    int save(Applications application);

    int update(Applications application);

    int delete(Integer id);

    void deleteByPatient(Integer patientId, Integer id);

    List<Applications> searchApplications(String query);

}
