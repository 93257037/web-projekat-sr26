package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Manufacturer;
import com.example.WEBSR26.model.Vaccine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VaccineRepository implements IVaccineRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private IManufacturerRepository manufacturerRepository;

    private class VaccineRowCallBackHandler implements RowCallbackHandler {

        private List<Vaccine> vaccines = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int index = 1;
            int id = rs.getInt(index++);
            String name = rs.getString(index++);
            int stock = rs.getInt(index++);
            int idManufacturer = rs.getInt(index);

            Manufacturer manufacturer = manufacturerRepository.findOne((long) idManufacturer);

            Vaccine vaccine = new Vaccine(id, name, stock,idManufacturer, manufacturer);

//            Vaccine vaccine = new Vaccine();
//            vaccine.setId(id);
//            vaccine.setName(name);
//            vaccine.setStock(stock);
//            vaccine.setIdManufacturer(idManufacturer);

            vaccines.add(vaccine);
        }

        public List<Vaccine> getVaccines() {
            return vaccines;
        }
    }

    @Override
    public List<Vaccine> findAllVaccine() {
        String sql = "select * from vaccine";
        VaccineRowCallBackHandler rowCallbackHandler = new VaccineRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getVaccines();
    }

    @Override
    public Vaccine findVaccineById(Long id) {
        String sql = "select * from vaccine where id = ?";
        VaccineRowCallBackHandler rowCallBackHandler = new VaccineRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler, id);
        return rowCallBackHandler.getVaccines().get(0);
    }

    @Override
    public int save(Vaccine vaccine) {
        String sql = "insert into vaccine (name, stock, idManufacturer) values (?, ?, ?)";
        return jdbcTemplate.update(sql, vaccine.getName(), vaccine.getStock(), vaccine.getIdManufacturer());
    }

    @Override
    public int update(Vaccine vaccine) {
        String sql = "update vaccine set name = ?, stock = ?, idManufacturer = ? where id = ?";
        return jdbcTemplate.update(sql, vaccine.getName(), vaccine.getStock(), vaccine.getIdManufacturer(), vaccine.getId());
    }

    @Transactional
    @Override
    public int delete(Long id) {
        String sql = "delete from vaccine where id = ?";
        return jdbcTemplate.update(sql, id);
    }
}