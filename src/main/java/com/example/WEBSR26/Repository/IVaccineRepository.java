package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.Vaccine;

import java.util.List;

public interface IVaccineRepository {
    public List<Vaccine> findAllVaccine();
    public Vaccine findVaccineById(Long id);
    public int save (Vaccine vaccine);
    public int update (Vaccine vaccine);
    public int delete(Long id);
}
