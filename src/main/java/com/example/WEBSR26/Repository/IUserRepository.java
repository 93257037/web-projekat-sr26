package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.User;

import java.util.List;

public interface IUserRepository {

    public List<User> findAll();
    public User findByEmailAndPassword(String email, String password);
    public User findOne(Long id);
    User findOne(String email);
    User findOne(String email, String password);
    public User save (User user);
    public User delete(Long id);
    public User update(User user);
}
