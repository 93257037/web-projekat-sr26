package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.TotalInfected;

import java.util.List;

public interface ITotalInfectedRepository {
    TotalInfected findOne(Integer id);

    List<TotalInfected> findAll();
}
