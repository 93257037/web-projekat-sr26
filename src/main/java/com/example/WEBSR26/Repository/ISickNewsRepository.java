package com.example.WEBSR26.Repository;

import com.example.WEBSR26.model.SickNews;

import java.util.List;

public interface ISickNewsRepository {
    SickNews findOne(Integer id);

    List<SickNews> findAll();

    int save(SickNews infectedNews);

    int update(SickNews infectedNews);

    int delete(Integer id);
}
