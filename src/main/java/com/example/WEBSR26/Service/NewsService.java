package com.example.WEBSR26.Service;

import com.example.WEBSR26.Repository.INewsRepository;
import com.example.WEBSR26.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsService {

    private final INewsRepository newsRepository;

    @Autowired
    public NewsService(INewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    public List<News> findAllNews() {
        return newsRepository.findAll();
    }

    public News findNewsById(Integer id) {
        return newsRepository.findOne(id);
    }

    public int save(News news) {
        return newsRepository.save(news);
    }

    public int update(News news) {
        return newsRepository.update(news);
    }

    public int delete(Integer id) {
        return newsRepository.delete(id);
    }
}
