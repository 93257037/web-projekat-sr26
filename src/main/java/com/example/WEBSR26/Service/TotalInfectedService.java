package com.example.WEBSR26.Service;

import com.example.WEBSR26.Repository.ITotalInfectedRepository;
import com.example.WEBSR26.model.TotalInfected;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TotalInfectedService {

    private final ITotalInfectedRepository totalInfectedRepository;

    @Autowired
    public TotalInfectedService(ITotalInfectedRepository totalInfectedRepository) {
        this.totalInfectedRepository = totalInfectedRepository;
    }

    public TotalInfected findOneTotalInfected(Integer id) {
        return totalInfectedRepository.findOne(id);
    }

    public List<TotalInfected> findAllTotalInfected() {
        return totalInfectedRepository.findAll();
    }

}
