package com.example.WEBSR26.Service;

import com.example.WEBSR26.Exception.UserNotFoundException;
import com.example.WEBSR26.Repository.IUserRepository;
import com.example.WEBSR26.Repository.UserRepository;
import com.example.WEBSR26.model.Role;
import com.example.WEBSR26.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.util.List;

@Service
public class UserService implements IUserRepository {

    @Autowired
    private IUserRepository userRepository;


    @Override
    public User findOne(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findOne(String email) {
        return null;
    }

    @Override
    public User findOne(String email, String password) {
        return null;
    }

    @Override
    public List<User> findAll() {return(List<User>) userRepository.findAll();}


    @Override
    public User findByEmailAndPassword(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email, password);
        return user;
    }


    @Override
    public User save(User user) {
        userRepository.save(user);
        return user;
    }


    @Override
    public User update(User user) {
        userRepository.update(user);
        return user;
    }
    public User get(String email) throws UserNotFoundException {
        return userRepository.findOne(email);
    }

    public User get(String email,String password) throws UserNotFoundException {
        return userRepository.findOne(email,password);
    }




    @Override
    public User delete(Long id) {
        User user = userRepository.findOne(id);

        if (user != null) {
            userRepository.delete(id);
        }

        return user;
    }

    public boolean checkCookies(Cookie[] cookies, Role role) throws UserNotFoundException {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getValue().contains("@")){
                    User temp = this.get(cookie.getValue());
                    return temp.getRole().equals(role);
                }
            }
        }
        return false;
    }

    public User checkCookieUser(Cookie[] cookies) throws UserNotFoundException {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if(cookie.getValue().contains("@")){
                    return this.get(cookie.getValue());
                }
            }
        }
        return null;
    }
}
