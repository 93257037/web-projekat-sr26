package com.example.WEBSR26.Service;

import com.example.WEBSR26.Repository.IApplicationsRepository;
import com.example.WEBSR26.model.Applications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationsService {

    @Autowired
    private IApplicationsRepository applicationsRepository;

    @Autowired
    public ApplicationsService(IApplicationsRepository applicationsRepository) {
        this.applicationsRepository = applicationsRepository;
    }

    public List<Applications> findAllApplications() {
        return applicationsRepository.findAll();
    }

    public Applications findApplicationById(Integer id) {
        return applicationsRepository.findOne(id);
    }

    public int save(Applications application) {
        return applicationsRepository.save(application);
    }

    public int update(Applications application) {
        return applicationsRepository.update(application);
    }

    public int delete(Integer id) {
        return applicationsRepository.delete(id);
    }

    public void deleteByPatient(Integer patientId, Integer id) {
        applicationsRepository.deleteByPatient(patientId, id);
    }

    public List<Applications> searchApplications(String query) {
        return applicationsRepository.searchApplications(query);
    }
}
