package com.example.WEBSR26.Service;

import com.example.WEBSR26.Repository.IPatientRepository;
import com.example.WEBSR26.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

    private final IPatientRepository patientRepository;

    @Autowired
    public PatientService(IPatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> findAllPatients() {
        return patientRepository.findAll();
    }

    public Patient findPatientById(Integer id) {
        return patientRepository.findOne(id);
    }

    public int save(Patient patient) {
        return patientRepository.save(patient);
    }

    public int update(Patient patient) {
        return patientRepository.update(patient);
    }

    public int delete(Integer id) {
        return patientRepository.delete(id);
    }
}
