package com.example.WEBSR26.Service;

import com.example.WEBSR26.Repository.ISickNewsRepository;
import com.example.WEBSR26.Repository.SickNewsRepository;
import com.example.WEBSR26.Repository.UserRepository;
import com.example.WEBSR26.model.SickNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class SickNewsService {

    private final ISickNewsRepository sickNewsRepository;

    @Autowired
    public SickNewsService(ISickNewsRepository sickNewsRepository) {
        this.sickNewsRepository = sickNewsRepository;
    }

    public SickNews findOneSickNews(Integer id) {
        return sickNewsRepository.findOne(id);
    }

    public List<SickNews> findAllSickNews() {
        return sickNewsRepository.findAll();
    }

    public int save(SickNews sickNews) {
        return sickNewsRepository.save(sickNews);
    }

    public int update(SickNews sickNews) {
        return sickNewsRepository.update(sickNews);
    }

    public int delete(Integer id) {
        return sickNewsRepository.delete(id);
    }
    public List<SickNews> getToday() {
        List<SickNews> news = sickNewsRepository.findAll();
        List<SickNews> newsToday = new ArrayList<>();
        for (SickNews i : news) {
            LocalDate newsDate = i.getDate().toLocalDate(); // Assuming 'getDateTime()' returns a java.util.Date

            if (newsDate.isEqual(LocalDate.now())) {
                newsToday.add(i);
            }
        }
        return newsToday;
    }
}
