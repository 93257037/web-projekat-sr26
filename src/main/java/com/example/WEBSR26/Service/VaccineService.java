package com.example.WEBSR26.Service;

import com.example.WEBSR26.Repository.IVaccineRepository;
import com.example.WEBSR26.model.Vaccine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VaccineService {

    private final IVaccineRepository vaccineRepository;

    @Autowired
    public VaccineService(IVaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    public List<Vaccine> findAllVaccines() {
        return vaccineRepository.findAllVaccine();
    }

    public Vaccine findVaccineById(Long id) {
        return vaccineRepository.findVaccineById(id);
    }

    public int save(Vaccine vaccine) {
        return vaccineRepository.save(vaccine);
    }

    public int update(Vaccine vaccine) {
        return vaccineRepository.update(vaccine);
    }

    public int delete(Long id) {
        return vaccineRepository.delete(id);
    }
}
