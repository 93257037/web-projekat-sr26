package com.example.WEBSR26.Service;

import com.example.WEBSR26.Repository.IManufacturerRepository;
import com.example.WEBSR26.model.Manufacturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManufacturerService {

    private final IManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerService(IManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    public List<Manufacturer> findAllManufacturers() {
        return manufacturerRepository.findAllManufacturer();
    }

    public Manufacturer findManufacturerById(Long id) {
        return manufacturerRepository.findOne(id);
    }

    public int save(Manufacturer manufacturer) {
        return manufacturerRepository.save(manufacturer);
    }

    public int update(Manufacturer manufacturer) {
        return manufacturerRepository.update(manufacturer);
    }

    public int delete(Long id) {
        return manufacturerRepository.delete(id);
    }
}
