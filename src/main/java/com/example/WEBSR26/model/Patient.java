package com.example.WEBSR26.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Patient {

    private Long patientId;
    private boolean vaccinated;
    private Integer doseAdministered;

    private LocalDate lastDose;
    private User user;
    private int userId;


    public Patient() {
    }

    public Patient(Long patientId,boolean vaccinated, Integer doseAdministered,  LocalDate lastDose, User user,int userId) {
        this.patientId = patientId;
        this.vaccinated = vaccinated;
        this.doseAdministered = doseAdministered;
        this.lastDose = lastDose;
        this.user = user;
        this.userId = userId;

    }

    public Patient(User newUser) {
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Integer getDoseAdministered() {
        return doseAdministered;
    }

    public void setDoseAdministered(Integer doseAdministered) {
        this.doseAdministered = doseAdministered;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isVaccinated() {
        return vaccinated;
    }

    public void setVaccinated(boolean vaccinated) {
        this.vaccinated = vaccinated;
    }

    public LocalDate getLastDose() {
        return lastDose;
    }

    public void setLastDose(LocalDate lastDose) {
        this.lastDose = lastDose;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setLastDoseFormatted(String string) {
    }
}
