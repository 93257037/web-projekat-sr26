package com.example.WEBSR26.model;

public enum Role {
    ADMIN, STAFF, PATIENT
}
