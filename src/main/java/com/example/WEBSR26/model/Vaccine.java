package com.example.WEBSR26.model;

public class Vaccine {
    private int id;
    private String name;
    private int stock;
    private Manufacturer manufacturer;
    private int idManufacturer;

    public Vaccine() {
    }


    public Vaccine(int id, String name, int stock, int idManufacturer, Manufacturer manufacturer) {
        this.id = id;
        this.name = name;
        this.stock = stock;
        this.idManufacturer = idManufacturer;
        this.manufacturer = manufacturer;
    }

//    public Vaccine(int id, String name, int stock, Manufacturer manufacturer) {
//    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getIdManufacturer() {
        return idManufacturer;
    }

    public void setIdManufacturer(int idManufacturer) {
        this.idManufacturer = idManufacturer;
    }
}
