package com.example.WEBSR26.model;

import java.time.LocalDateTime;

public class SickNews  {
    private int id;
    private int numberSick;
    private int tested;
    private int hospitalized;
    private int onRespirator;
    private LocalDateTime date;



    public SickNews() {
    }

    public SickNews(int id, int numberSick, int tested, int hospitalized, int onRespirator, LocalDateTime date) {
        this.id = id;
        this.numberSick = numberSick;
        this.tested = tested;
        this.hospitalized = hospitalized;
        this.onRespirator = onRespirator;
        this.date = date;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberSick() {
        return numberSick;
    }

    public void setNumberSick(int numberSick) {
        this.numberSick = numberSick;
    }

    public int getTested() {
        return tested;
    }

    public void setTested(int tested) {
        this.tested = tested;
    }

    public int getHospitalized() {
        return hospitalized;
    }

    public void setHospitalized(int hospitalized) {
        this.hospitalized = hospitalized;
    }

    public int getOnRespirator() {
        return onRespirator;
    }

    public void setOnRespirator(int onRespirator) {
        this.onRespirator = onRespirator;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }


}
