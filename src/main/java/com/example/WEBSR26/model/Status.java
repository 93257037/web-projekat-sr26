package com.example.WEBSR26.model;

public enum Status {
    SENT,
    ACCEPTED,
    DENIED,
    REVISION
}
