package com.example.WEBSR26.model;

import java.time.LocalDateTime;

public class TotalInfected {
    private int id;
    private int TotalInfected;
    private LocalDateTime lastUpdated;

    public TotalInfected() {
    }

    public TotalInfected(int id, int totalInfected, LocalDateTime lastUpdated) {
        this.id = id;
        TotalInfected = totalInfected;
        this.lastUpdated = lastUpdated;
    }

    public int getTotalInfected() {
        return TotalInfected;
    }

    public void setTotalInfected(int totalInfected) {
        TotalInfected = totalInfected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
