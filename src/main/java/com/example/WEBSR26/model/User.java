package com.example.WEBSR26.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class User {

    private Long id;
    private String name;
    private String surName;
    private String password;
    private String jmbg;
    private String email;
    private String address;
    private String phoneNumber;
    private Role role;
    private LocalDateTime regDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
    private boolean isLogged;

    public User() {
    }

    public User(Long id, String name, String surName, String password, String jmbg, String email, String address, String phoneNumber, Role role, LocalDateTime regDate, LocalDate dateOfBirth, boolean isLogged) {
        this.id = id;
        this.name = name;
        this.surName = surName;
        this.password = password;
        this.jmbg = jmbg;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.regDate = regDate;
        this.dateOfBirth = dateOfBirth;
        this.isLogged = isLogged;
    }

    public User(Long id, String name, String surName, String password, String jmbg, String email, String address, String phoneNumber, Role role, LocalDateTime regDate, LocalDate dateOfBirth) {
        this.id = id;
        this.name = name;
        this.surName = surName;
        this.password = password;
        this.jmbg = jmbg;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.regDate = regDate;
        this.dateOfBirth = dateOfBirth;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public LocalDateTime getRegDate() {
        return regDate;
    }

    public void setRegDate(LocalDateTime regDate) {
        this.regDate = regDate;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }

}
