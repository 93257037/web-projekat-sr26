package com.example.WEBSR26.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Applications {
    private Long id;
    private LocalDate dateTime;
    private int patientId;
    private Patient patient;
    private int vaccineId;
    private Vaccine vaccine;

    public Applications(){

    }

    public Applications(Long id, LocalDateTime dateTime, int patientId, int vaccineId) {
        this.id = id;
        this.dateTime = LocalDate.from(dateTime);
        this.patientId = patientId;
        this.vaccineId = vaccineId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDate dateTime) {
        this.dateTime = dateTime;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getVaccineId() {
        return vaccineId;
    }

    public void setVaccineId(int vaccineId) {
        this.vaccineId = vaccineId;
    }
    public void setPatient(Patient patient){
        this.patient = patient;
        this.patientId = patientId;
    }

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
        this.vaccineId = vaccine.getId();

    }
}
