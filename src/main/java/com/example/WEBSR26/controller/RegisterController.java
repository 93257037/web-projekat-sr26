package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Exception.UserNotFoundException;
import com.example.WEBSR26.Repository.UserRepository;
import com.example.WEBSR26.Service.PatientService;
import com.example.WEBSR26.Service.UserService;
import com.example.WEBSR26.model.Patient;
import com.example.WEBSR26.model.Role;
import com.example.WEBSR26.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.Set;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private UserService userService;


    @GetMapping
    public String showRegistrationForm(Model model) {
        model.addAttribute("pageTitle", "Register User");
        model.addAttribute("user", new User());
        model.addAttribute("method", "/register/save");  // Corrected form action

        return "register";
    }


    @PostMapping("/save")
    public String saveUser(User user, RedirectAttributes ra) throws UserNotFoundException {
        user.setRegDate(LocalDateTime.now());
        user.setName("MARKO");

        if (user.getName() == null || user.getName().isEmpty()) {
            throw new IllegalArgumentException("The name field cannot be empty.");
        }
        user.setRegDate(LocalDateTime.now());
        userService.save(user);
        User newUser = userService.findOne(user.getEmail());

        if(user.getRole() == Role.PATIENT){
            Patient patient = new Patient(newUser);
            patientService.save(patient);

        }
        ra.addFlashAttribute("message", "Korisnik je sacuvan");
        return "redirect:/";
    }

}
