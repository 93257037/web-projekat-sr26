package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Service.NewsService;
import com.example.WEBSR26.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/news")
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping
    public String listNews(Model model) {
        List<News> newsList = newsService.findAllNews();
        model.addAttribute("newsList", newsList);
        return "news";
    }

    @GetMapping("/{id}")
    public String showNewsDetails(@PathVariable Integer id, Model model) {
        News news = newsService.findNewsById(id);
        model.addAttribute("news", news);
        return "newsDetails";
    }

    @GetMapping("/create")
    public String showCreateNewsForm(Model model) {
        model.addAttribute("news", new News());
        return "addNews";
    }

    @PostMapping("/create")
    public String createNews(@ModelAttribute News news) {
        news.setDate(LocalDateTime.now());
        newsService.save(news);
        return "redirect:/news";
    }

    @GetMapping("/edit/{id}")
    public String showEditNewsForm(@PathVariable Integer id, Model model) {
        News news = newsService.findNewsById(id);
        model.addAttribute("news", news);
        return "editNews";
    }

    @PostMapping("/edit/{id}")
    public String editNews(@PathVariable Integer id, @ModelAttribute News news) {
        news.setId(id); // Set the ID for the edited news
        news.setDate(LocalDateTime.now());
        newsService.update(news);
        return "redirect:/news";
    }

    @GetMapping("/delete/{id}")
    public String deleteNews(@PathVariable Integer id) {
        newsService.delete(id);
        return "redirect:/news";
    }
}
