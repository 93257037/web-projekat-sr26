package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Service.VaccineService;
import com.example.WEBSR26.model.Vaccine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/vaccine")
public class VaccineController {

    private final VaccineService vaccineService;

    @Autowired
    public VaccineController(VaccineService vaccineService) {
        this.vaccineService = vaccineService;
    }

    @GetMapping
    public String listVaccines(Model model) {
        List<Vaccine> vaccines = vaccineService.findAllVaccines();
        model.addAttribute("vaccines", vaccines);
        return "vaccine";
    }

    @GetMapping("/{id}")
    public String showVaccineDetails(@PathVariable Long id, Model model) {
        Vaccine vaccine = vaccineService.findVaccineById(id);
        model.addAttribute("vaccine", vaccine);
        return "vaccineDetails";
    }

    @GetMapping("/create")
    public String showCreateVaccineForm(Model model) {
        model.addAttribute("vaccine", new Vaccine());
        return "addVaccine";
    }

    @PostMapping("/create")
    public String createVaccine(@ModelAttribute Vaccine vaccine) {
        vaccineService.save(vaccine);
        return "redirect:/vaccine";
    }

    @GetMapping("/edit/{id}")
    public String showEditVaccineForm(@PathVariable Long id, Model model) {
        Vaccine vaccine = vaccineService.findVaccineById(id);
        model.addAttribute("vaccine", vaccine);
        return "editVaccine";
    }

    @PostMapping("/edit/{id}")
    public String editVaccine(@PathVariable int id, @ModelAttribute Vaccine vaccine) {
        vaccine.setId(id); // Set the ID for the edited vaccine
        vaccineService.update(vaccine);
        return "redirect:/vaccine";
    }

    @GetMapping("/delete/{id}")
    public String deleteVaccine(@PathVariable Long id) {
        vaccineService.delete(id);
        return "redirect:/vaccine";
    }
}
