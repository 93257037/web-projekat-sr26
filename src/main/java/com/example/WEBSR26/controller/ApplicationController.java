package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Exception.UserNotFoundException;
import com.example.WEBSR26.Service.ApplicationsService;
import com.example.WEBSR26.Service.PatientService;
import com.example.WEBSR26.Service.UserService;
import com.example.WEBSR26.Service.VaccineService;
import com.example.WEBSR26.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/applications")
public class ApplicationController {

    private final ApplicationsService applicationService;
    @Autowired
    private  UserService userService;
    @Autowired
    private  VaccineService vaccineService;
    @Autowired
    private  PatientService patientService;

    public ApplicationController(PatientService patientService, UserService userService, VaccineService vaccineService, ApplicationsService applicationService) {
        this.patientService = patientService;
        this.userService = userService;
        this.vaccineService = vaccineService;
        this.applicationService = applicationService;
    }

    @PostMapping("/application/new")
    public String saveApplication(@RequestParam("vaccineId") Long vaccineId, HttpServletRequest request, RedirectAttributes ra) throws UserNotFoundException {
        Vaccine vaccine = vaccineService.findVaccineById(vaccineId);

        Cookie[] cookies = request.getCookies();
        User user = userService.checkCookieUser(cookies);

        if(user.getRole().equals(Role.PATIENT)){
            Patient patient = patientService.findPatientById(user.getId());

            Applications application = new Applications();

            application.setDateTime(LocalDate.now());
            application.setPatient(patient);
            application.setVaccine(vaccine);

            applicationService.save(application);

            ra.addFlashAttribute("message", "application submitted");
        } else{
            ra.addFlashAttribute("message", "application failed");
        }
        return "redirect:/patient/vax";
    }


    @GetMapping("/{id}")
    public String showApplicationDetails(@PathVariable int id, Model model) {
        Applications application = applicationService.findApplicationById(id);
        model.addAttribute("application", application);
        return "applications/details";
    }

    @GetMapping("/create")
    public String showCreateForm() {
        return "applications/create";
    }

    @PostMapping("/create")
    public String createApplication(@ModelAttribute Applications application) {
        applicationService.save(application);
        return "redirect:/applications";
    }

    @GetMapping("/{id}/edit")
    public String showEditForm(@PathVariable int id, Model model) {
        Applications application = applicationService.findApplicationById(id);
        model.addAttribute("application", application);
        return "applications/edit";
    }

    @PostMapping("/{id}/edit")
    public String editApplication(@PathVariable int id, @ModelAttribute Applications application) {
        applicationService.update(application);
        return "redirect:/applications";
    }

    @GetMapping("/{id}/delete")
    public String showDeleteConfirmation(@PathVariable int id, Model model) {
        Applications application = applicationService.findApplicationById(id);
        model.addAttribute("application", application);
        return "applications/delete";
    }

    @PostMapping("/{id}/delete")
    public String deleteApplication(@PathVariable int id) {
        applicationService.delete(id);
        return "redirect:/applications";
    }
}
