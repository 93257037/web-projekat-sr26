package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Service.PatientService;
import com.example.WEBSR26.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/patients")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping
    public String listPatients(Model model) {
        List<Patient> patientList = patientService.findAllPatients();
        model.addAttribute("patientList", patientList);
        return "patients";
    }

    @GetMapping("/{id}")
    public String showPatientDetails(@PathVariable Integer id, Model model) {
        Patient patient = patientService.findPatientById(id);
        model.addAttribute("patient", patient);
        return "patientDetails";
    }

    @GetMapping("/create")
    public String showCreatePatientForm(Model model) {
        model.addAttribute("patient", new Patient());
        return "addPatient";
    }

    @PostMapping("/create")
    public String createPatient(@ModelAttribute Patient patient) {
        patientService.save(patient);
        return "redirect:/patients";
    }

    @GetMapping("/edit/{id}")
    public String showEditPatientForm(@PathVariable Integer id, Model model) {
        Patient patient = patientService.findPatientById(id);
        patient.setLastDoseFormatted(patient.getLastDose().toString()); // Format the date as a string
        model.addAttribute("patient", patient);
        return "editPatient";
    }



    @PostMapping("/edit/{id}")
    public String editPatient(@PathVariable Long id, @ModelAttribute Patient patient) {
        patient.setPatientId(id); // Set the ID for the edited patient
        patientService.update(patient);
        return "redirect:/patients";
    }

    @GetMapping("/delete/{id}")
    public String deletePatient(@PathVariable Integer id) {
        patientService.delete(id);
        return "redirect:/patients";
    }
}
