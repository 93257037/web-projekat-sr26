package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Service.TotalInfectedService;
import com.example.WEBSR26.model.TotalInfected;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/totalinfected")
public class TotalInfectedController {

    private final TotalInfectedService totalInfectedService;

    @Autowired
    public TotalInfectedController(TotalInfectedService totalInfectedService) {
        this.totalInfectedService = totalInfectedService;
    }

    @GetMapping
    public String listTotalInfected(Model model) {
        List<TotalInfected> totalInfectedList = totalInfectedService.findAllTotalInfected();
        model.addAttribute("totalInfectedList", totalInfectedList);
        return "totalinfected";
    }

    @GetMapping("/{id}")
    public String showTotalInfectedDetails(@PathVariable Integer id, Model model) {
        TotalInfected totalInfected = totalInfectedService.findOneTotalInfected(id);
        model.addAttribute("totalInfected", totalInfected);
        return "totalinfectedDetails";
    }




}
