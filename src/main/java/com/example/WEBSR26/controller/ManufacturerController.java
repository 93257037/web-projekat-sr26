package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Service.ManufacturerService;
import com.example.WEBSR26.model.Manufacturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/manufacturer")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public String listManufacturers(Model model) {
        List<Manufacturer> manufacturers = manufacturerService.findAllManufacturers();
        model.addAttribute("manufacturers", manufacturers);
        return "manufacturer";
    }

    @GetMapping("/{id}")
    public String showManufacturerDetails(@PathVariable Long id, Model model) {
        Manufacturer manufacturer = manufacturerService.findManufacturerById(id);
        model.addAttribute("manufacturer", manufacturer);
        return "manufacturerDetails";
    }

    @GetMapping("/create")
    public String showCreateManufacturerForm(Model model) {
        model.addAttribute("manufacturer", new Manufacturer());
        return "addManufacturer";
    }

    @PostMapping("/create")
    public String createManufacturer(@ModelAttribute Manufacturer manufacturer) {
        manufacturerService.save(manufacturer);
        return "redirect:/manufacturer";
    }

    @GetMapping("/edit/{id}")
    public String showEditManufacturerForm(@PathVariable Long id, Model model) {
        Manufacturer manufacturer = manufacturerService.findManufacturerById(id);
        model.addAttribute("manufacturer", manufacturer); // Make sure the attribute name matches
        return "editManufacturer";
    }



    @PostMapping("/edit/{id}")
    public String editManufacturer(@PathVariable Long id, @ModelAttribute Manufacturer manufacturer) {
        manufacturer.setId(id); // Set the ID for the edited manufacturer
        manufacturerService.update(manufacturer);
        return "redirect:/manufacturer";
    }

    @GetMapping("/delete/{id}")
    public String deleteManufacturer(@PathVariable Long id) {
        manufacturerService.delete(id);
        return "redirect:/manufacturer";
    }
}
