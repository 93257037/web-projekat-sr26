package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Exception.UserNotFoundException;
import com.example.WEBSR26.Repository.NewsRepository;
import com.example.WEBSR26.Repository.TotalInfectedRepository;
import com.example.WEBSR26.Repository.UserRepository;
import com.example.WEBSR26.Service.SickNewsService;
import com.example.WEBSR26.Service.UserService;
import com.example.WEBSR26.model.News;
import com.example.WEBSR26.model.Role;
import com.example.WEBSR26.model.SickNews;
import com.example.WEBSR26.model.TotalInfected;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class IndexController {

    private final NewsRepository service;

    private final SickNewsService serviceInfected;

    private final TotalInfectedRepository totalInfectedRepository;

    private final UserService userService;

    public IndexController(NewsRepository service, SickNewsService serviceInfected, UserService userService, TotalInfectedRepository totalInfectedRepository) {
        this.service = service;
        this.serviceInfected = serviceInfected;
        this.userService = userService;
        this.totalInfectedRepository = totalInfectedRepository;

    }


    @GetMapping("/")
    public String showIndex(Model model, HttpServletRequest request) throws UserNotFoundException {
        List<News> list = service.findAll();
        model.addAttribute("listNews", list);

        List<SickNews> infToday = serviceInfected.getToday();
        model.addAttribute("numberSick", infToday);

        List<TotalInfected> infTotal = totalInfectedRepository.findAll();
        model.addAttribute("totalInfected", infTotal);

        Cookie[] cookies = request.getCookies();
        if(userService.checkCookies(cookies, Role.ADMIN)){
            model.addAttribute("role", "admin");
        }
        else if(userService.checkCookies(cookies, Role.STAFF)){
            model.addAttribute("role", "staff");
        }
        else if(userService.checkCookies(cookies, Role.PATIENT)){
            model.addAttribute("role", "patient");
        } else{
            model.addAttribute("role", "none");
        }

        return "index";
    }
}