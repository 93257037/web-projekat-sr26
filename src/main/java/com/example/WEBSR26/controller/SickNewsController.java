package com.example.WEBSR26.Controller;

import com.example.WEBSR26.Service.SickNewsService;
import com.example.WEBSR26.model.SickNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/sicknews")
public class SickNewsController {

    private final SickNewsService sickNewsService;

    @Autowired
    public SickNewsController(SickNewsService sickNewsService) {
        this.sickNewsService = sickNewsService;
    }

    @GetMapping
    public String listSickNews(Model model) {
        List<SickNews> sickNewsList = sickNewsService.findAllSickNews();
        model.addAttribute("sickNewsList", sickNewsList);
        return "sicknews";
    }

    @GetMapping("/{id}")
    public String showSickNewsDetails(@PathVariable Integer id, Model model) {
        SickNews sickNews = sickNewsService.findOneSickNews(id);
        model.addAttribute("sickNews", sickNews);
        return "sicknewsDetails";
    }

    @GetMapping("/create")
    public String showCreateSickNewsForm(Model model) {
        model.addAttribute("sickNews", new SickNews());
        return "addSickNews";
    }

    @PostMapping("/create")
    public String createSickNews(@ModelAttribute SickNews sickNews) {
        sickNews.setDate(LocalDateTime.now()); // Set the current date and time
        sickNewsService.save(sickNews); // Save the instance with the updated date
        return "redirect:/sicknews";
    }


    @GetMapping("/edit/{id}")
    public String showEditSickNewsForm(@PathVariable Integer id, Model model) {
        SickNews sickNews = sickNewsService.findOneSickNews(id);
        model.addAttribute("sickNews", sickNews);
        return "editSickNews";
    }

    @PostMapping("/edit/{id}")
    public String editSickNews(@PathVariable Integer id, @ModelAttribute SickNews sickNews) {
        sickNews.setId(id); // Set the ID for the edited sick news
        sickNewsService.update(sickNews);
        return "redirect:/sicknews";
    }

    @GetMapping("/delete/{id}")
    public String deleteSickNews(@PathVariable Integer id) {
        sickNewsService.delete(id);
        return "redirect:/sicknews";
    }
}
